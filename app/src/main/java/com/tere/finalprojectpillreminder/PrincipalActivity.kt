
package com.tere.finalprojectpillreminder

import androidx.appcompat.app.AppCompatActivity

import android.os.Bundle


class PrincipalActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
    }

}