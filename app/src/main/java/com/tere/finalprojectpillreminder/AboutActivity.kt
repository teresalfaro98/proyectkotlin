
package com.tere.finalprojectpillreminder

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.widget.TextView

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val version = packageManager.getPackageInfo(packageName, 0).versionName
        val versionMsg = String.format(getString(R.string.about_version), version)

        val contents = findViewById<TextView>(R.id.version)!!
        contents.setText(versionMsg)
        contents.movementMethod = LinkMovementMethod.getInstance()
    }
}
