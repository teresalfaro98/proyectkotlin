package com.tere.finalprojectpillreminder

import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.SystemClock
import android.text.TextUtils
import android.text.format.DateFormat
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.firebase.firestore.FirebaseFirestore
import com.tere.finalprojectpillreminder.auth.AuthActivity
import java.util.*


var TAG = "FirebaseDebug"
val db = FirebaseFirestore.getInstance()
class MainActivity : AppCompatActivity(), SharedPreferencesListener {

    private var resetMenuItem: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        finish()
        startActivity(Intent(this, AuthActivity::class.java))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        resetMenuItem = menu.findItem(R.id.action_reset)
        resetMenuItem!!.isVisible = DataModel.hasTakenDrugToday()
        return true
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key.equals(DataModel.DRUG_TAKEN_TIMESTAMP)) {
            updateBody(true)
        }
    }

    override fun onStart() {
        super.onStart()
        updateBody(false)
        DataModel.addListener(this)
    }

    override fun onResume() {
        super.onResume()
        checkForMissedAlarms()
    }

    override fun onStop() {
        super.onStop()
        DataModel.removeListener(this)
    }

    private fun updateBody(useFade: Boolean) {
        val drugTaken = DataModel.hasTakenDrugToday()
        val fragment =
            if (drugTaken) { MedicineTakenFragment()    }
            else           { MedicineNotTakenFragment() }

        resetMenuItem?.isVisible = drugTaken
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment_container, fragment)
            if (useFade) {
                setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            }
            commit()
        }
    }

    private fun checkForMissedAlarms() {

        if (!DataModel.reminderIsEnabled()) { return }

        val nowTimestamp = Calendar.getInstance().timeInMillis
        val nextAlarmTimestamp = DataModel.getNextAlarmTimestamp()
        if (nowTimestamp - 1*60*1000L < nextAlarmTimestamp) { return }

        val timeSinceBoot = SystemClock.elapsedRealtime()
        if (timeSinceBoot < 5*60*1000L) { return }

        AlertDialog.Builder(this)
            .setTitle(R.string.missed_reminder_title)
            .setIcon(R.drawable.ic_pill)
            .setMessage(R.string.missed_reminder_message)
            .setPositiveButton(R.string.missed_reminder_button) { _, _ -> resetAlarms() }
            .setOnDismissListener { resetAlarms() }
            .show()
    }

    private fun resetAlarms() {
        Notifications.resetAlarm()
    }

    private fun resetMedicineTakenTime() {
        val dialog = AlertDialog.Builder(this)
            .setTitle(R.string.reset_confirmation_title)
            .setMessage(R.string.reset_confirmation_message)
            .setPositiveButton(R.string.reset_confirmation_ok) { _, _ ->
                DataModel.unsetDrugTakenTimestamp()
            }
            .setNegativeButton(R.string.reset_confirmation_cancel,null)
            .create()
        dialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_reset -> {
                resetMedicineTakenTime()
                return true
            }
            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
            }
            R.id.action_about -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
            }
        }
        // Fallback
        return super.onOptionsItemSelected(item)
    }

    class MedicineNotTakenFragment : Fragment() {

        private lateinit var button: Button

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val root = inflater.inflate(R.layout.fragment_medicine_not_taken, container, false)
            button = root.findViewById(R.id.button)
            button.setOnClickListener { clickButton() }
            return root
        }

        private fun clickButton() {
            DataModel.takeDrugNow()
        }
    }

    class MedicineTakenFragment : Fragment() {

        private lateinit var drugTakenMessageView: TextView
        private lateinit var bannerView: View
        private lateinit var bannerButton: Button

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val root = inflater.inflate(R.layout.fragment_medicine_taken, container, false)
            drugTakenMessageView = root.findViewById(R.id.drug_taken_message)
            bannerView = root.findViewById(R.id.banner)
            bannerButton = root.findViewById(R.id.banner_button)

            bannerButton.setOnClickListener { gotoSettings() }
            return root
        }

        override fun onResume() {
            super.onResume()

            // The non-breaking space avoids a line-break between 6:00 and AM
            val timestamp = DataModel.getDrugTakenTimestamp()
            val timeStr = DateFormat.getTimeFormat(activity).format(timestamp)
            val nbspTimeStr = timeStr.replace(" ", "\u00A0" )

            val weekday = DateFormat.format("EEEE", timestamp)

            val drugTakenMessage = getString(R.string.drug_taken_message)
            drugTakenMessageView.text = String.format(drugTakenMessage, weekday, nbspTimeStr)

            bannerView.visibility =
                if (DataModel.reminderIsEnabled()) { View.GONE } else { View.VISIBLE }
        }

        private fun gotoSettings() {
            val intent = Intent(requireActivity(), SettingsActivity::class.java)
            startActivity(intent)
        }
    }
    /*private lateinit var txtUser: EditText
    private lateinit var txtPassword:EditText
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        txtUser=findViewById(R.id.txt_user)
        txtPassword=findViewById(R.id.txt_password)
        progressBar=findViewById(R.id.progressBar)
    }
    fun login(view:View){
        loginUser()
    }


    private fun loginUser(){
        val user:String=txtUser.text.toString()
        val password:String=txtPassword.text.toString()

        if(!TextUtils.isEmpty(user) && !TextUtils.isEmpty(password)){
            progressBar.visibility=View.VISIBLE

            db.collection("Users").whereEqualTo("email",user).get()
                .addOnSuccessListener {
                        result ->
                    for(document in result){
                        val intent = Intent(this, PrincipalActivity::class.java)
                        startActivity(intent)
                        }
                    }
                }
        }*/
}