package com.tere.finalprojectpillreminder.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import com.google.firebase.firestore.FirebaseFirestore
import com.tere.finalprojectpillreminder.PrincipalActivity
import com.tere.finalprojectpillreminder.R
import kotlinx.android.synthetic.main.fragment_register.*
import org.w3c.dom.Text

var TAG = "FirebaseDebug"
val db = FirebaseFirestore.getInstance()
class AuthActivity : AppCompatActivity() {
    private lateinit var txtUser: EditText
    private lateinit var txtPassword:EditText
    private lateinit var progressBar: ProgressBar
    private lateinit var txtName:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_auth)
    }
    fun login(view:View){
        loginUser()
    }
    private fun loginUser(){
        txtUser=findViewById(R.id.txt_email)
        txtPassword=findViewById(R.id.txt_password)
        progressBar=findViewById(R.id.progressBar)
        val user:String=txtUser.text.toString()
        val password:String=txtPassword.text.toString()

        if(!TextUtils.isEmpty(user) && !TextUtils.isEmpty(password)){
            progressBar.visibility=View.VISIBLE

            db.collection("Users").whereEqualTo("email",user).get()
                .addOnSuccessListener {
                    db.collection("Users").whereEqualTo("email",password).get().addOnSuccessListener {
                            result ->
                        for(document in result){
                            val intent = Intent(this, PrincipalActivity::class.java)
                            startActivity(intent)
                        }
                    }
                }
        }
    }
    fun register(view: View){
        val intent = Intent(this, RegisterFragment::class.java)
        startActivity(intent)
    }
    fun loginregister(view:View){
        registerUser()
    }

    private fun registerUser(){
        txtUser=findViewById(R.id.txt_email)
        txtPassword=findViewById(R.id.txt_password)
        txtName=findViewById(R.id.txt_userregister)

        val user:String=txtUser.text.toString()
        val password:String=txtPassword.text.toString()
        val name:String=txtName.text.toString()

        if(!TextUtils.isEmpty(user) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(name)){

            db.collection("Users").document().set(Users(name = name, password = password, user = user))
                .addOnSuccessListener {
                    val intent = Intent(this, PrincipalActivity::class.java)
                    startActivity(intent)
                }
        }
    }
    data class Users(var name:String = "",var password:String="", var user:String="")

}